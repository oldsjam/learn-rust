use std::io::{self, Write};

fn main() {
    print!("Please enter your name: ");
    let _ = io::stdout().flush();

    let mut name = String::new();

    io::stdin()
        .read_line(&mut name)
        .expect("Failed to read input.");

    match name.trim() {
        "Alice" | "Bob"    => println!("Your name is {}!", name.trim()),
        _                  => println!("Enter a valid name"),
    }

}
