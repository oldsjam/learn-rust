use std::io::{self, Write};

fn main() {
    print!("Enter a number: ");
    let _ = io::stdout().flush();

    let mut num_in = String::new();

    io::stdin()
        .read_line(&mut num_in)
        .expect("Failed to read input.");

    let parse_num = num_in.trim().parse::<u32>().ok();

    let number = match parse_num {
        Some(parse_num) => parse_num,
        None            => {
            println!("enter a number");
            return;
        }
    };

    let mut counter = 0;
    for x in 1..number+1 {
        counter += x;

    }

    println!("{}", counter);
}
