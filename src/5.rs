// bring both std::io AND std::io::Write in scope one statement
use std::io::{self, Write};

fn main() {
    // print to console, no newline, no flush
    print!("Enter a number: ");
    // flush the print buffer to screen
    // use _ pattern to ignore unused returned result
    let _ = io::stdout().flush();

    // bind an empty, heap allocated string to mutable num_in
    let mut num_in = String::new();

    // get user input from stdin
    //
    io::stdin()
        // returns a result that must be handled
        .read_line(&mut num_in)
         // handle exceptions
        .expect("Failed to read input.");

    // convert string input to u32
    let parse_num = num_in
        //
        .trim()
        .parse::<u32>()
        .ok();

    let number = match parse_num {
        Some(parse_num) => parse_num,
        None            => {
            println!("enter a number");
            return;
        }
    };

    let mut counter = 0;
    for x in 1..number+1 {
        if (x % 3 == 0) | (x % 5 == 0) {
            counter += x;
        }
    }

    println!("{}", counter);
}
