/*
 * Problems from https://adriann.github.io/programming_problems.html
 * Problem: 6
 */

use std::io::{self, Write};
use std::ascii::AsciiExt;

fn sum(input: u32) -> u32 {
    let mut accumulator = 0;
    for i in 1..input+1 {
        accumulator += i;
    }
    accumulator
}

fn product(input: u32) -> u32 {
    let mut accumulator = 0;
    for i in 1..input+1 {
        if i == 1 {accumulator += 1}
        accumulator *= i;
    }
    accumulator
}

fn main () {
    loop {
        print!("Would you like to find the \
        product, sum, or quit? ");
        let _ = io::stdout().flush();
        let mut operator = String::new();
        io::stdin()
        .read_line(&mut operator)
        .expect("failed to read from stdin");
        let operator = operator.to_ascii_lowercase();
        let operator = operator.trim();
        if operator == "quit" {
            break
        } else if (operator != "sum") & (operator != "product") {
            println!("Enter sum, product, or quit.");
            continue
        }

        print!("Please enter a number n: ");
        let _ = io::stdout().flush();
        let mut user_in = String::new();
        io::stdin()
        .read_line(&mut user_in)
        .expect("failed to read from stdin");
        let user_in: u32 = match user_in.trim().parse() {
            Ok(num)   => num,
            Err(_)  => {
                println!("Enter a valid number");
                continue
            }
        };

        match operator {
            "sum"   => {
                println!("{}", sum(user_in))
            }
            "product" => {
                println!("{}", product(user_in))
            }
            _   => println!("Enter a valid operator!")
        };
    }
}